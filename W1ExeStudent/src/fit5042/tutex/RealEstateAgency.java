package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.SimplePropertyRepositoryImpl;
import fit5042.tutex.repository.entities.Property;
import java.util.Scanner;
/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency extends SimplePropertyRepositoryImpl {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.setName(name);
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception {
        Property property01=new Property(1,"24 Boston Ave, Malvern East VIC 3145,Australia",2,150,420000.00);
        Property property02=new Property(2,"11 Bettina St,Clayton VIC 3168,Australia",3,352,360000.00);
        Property property03=new Property(3,"3 Wattle Ave,Glen Huntly VIC 3163,Australia",5,800,650000.00);
        Property property04=new Property(4,"3 Hamilton St,Bentleigh VIC 3204,Australia",2,170,435000.00);
        Property property05=new Property(5,"82 Spring Rd,Hampton East VIC 3188,Australia",1,60,820000.00);
        propertyRepository.addProperty(property01);
        propertyRepository.addProperty(property02);
        propertyRepository.addProperty(property03);
        propertyRepository.addProperty(property04);
        propertyRepository.addProperty(property05);
        System.out.println("5 properties added successfully");
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception {
       for(Property property:propertyRepository.getAllProperties()){
           System.out.println(property.toString());
       }
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception {
        System.out.println("Enter the ID of the property you want to search:");
        try(Scanner sc=new Scanner(System.in)){
        int id=sc.nextInt();
        Property p=propertyRepository.searchPropertyById(id);
        System.out.println(p.toString());
        }
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
