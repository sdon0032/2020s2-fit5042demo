/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;

import java.util.*;
/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
    private ArrayList<Property> properties;
    public SimplePropertyRepositoryImpl()  {
    	properties = new ArrayList<Property>();
    }

    @Override
    public void addProperty(Property property) {
        properties.add(property);
    }

    @Override
    public Property searchPropertyById(int id) {
    	Property p=null;
        for(Property property:properties){
            if (id==property.getId()){
                p=property;
            }
        }
        return p;
    }

    @Override
    public List<Property> getAllProperties() {
        return properties;
    }
}
