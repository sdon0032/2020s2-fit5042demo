package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;
@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	private Set<Property> compareList;
	
	public ComparePropertySessionBean() {
		compareList=new HashSet<>();
	}

	@Override
	public void addProperty(Property searchPropertyById) {
		compareList.add(searchPropertyById);
		
	}

	@Override
	public void removeProperty(Property searchPropertyById) {
		for(Property p:compareList) {
			if(p.getPropertyId()==searchPropertyById.getPropertyId()) {
				compareList.remove(p);
				break;
			}
		}
		
	}

	@Override
	public int bestPerRoom() {
		int bestId=0;
		int numberOfRooms;
		double price;
		double bestPerRoom=100000000.00;
		for(Property p:compareList) {
			numberOfRooms=p.getNumberOfBedrooms();
			price=p.getPrice();
			if(price/numberOfRooms<bestPerRoom) {
				bestPerRoom=price/numberOfRooms;
				bestId=p.getPropertyId();
			}
		}
		return bestId;
	}

	@Override
	public CompareProperty create() throws CreateException, RemoteException {
		return null;
	}
	
	@PostConstruct
	public void init() {
		compareList=new HashSet<>();
	}
	public void ejbCreate() throws CreateException{
		
	}
}
